import "../scss/app.scss";
import * as R from "ramda";

window.addEventListener("DOMContentLoaded", () => {
  // This block will be executed once the page is loaded and ready

  const arrayToPluck = [
    { name: "John", class: "is-primary" },
    { age: 23, class: "is-warning" },
    { job: "programmer", class: "is-danger" },
  ];
  
  let getClasses = R.pluck('class', arrayToPluck);
  console.log(getClasses);
  
  const articles = document.querySelectorAll("article");
  for (let index = 0; index < articles.length; index++) {
    articles[index].classList.add(getClasses[index]);
  }
});
